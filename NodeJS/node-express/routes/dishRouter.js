const express = require('express')

const dishRouter = express.Router()

dishRouter
  .route('/')
  .get((req, res, next) => {
    res.json({
      message: 'Will send all the dishes to you!',
    })
  })
  .post((req, res, next) => {
    res.status(201).json({
      message: `Will add the dish:  ${req.body.name} with details: ${req.body.description}`,
    })
  })

dishRouter
  .route('/:dishId')
  .get((req, res, next) => {
    res.status(200).json({
      dish: `ID: ${req.params.dishId} Name: Dish-Name and Description: Dish-Description`,
    })
  })
  .put((req, res, next) => {
    res.status(200).json({
      message: `Updating dish with id: ${req.params.dishId}`,
    })
  })
  .delete((req, res, next) => {
    res.status(200).json({
      message: `Deleting dish with id: ${req.params.dishId}`,
    })
  })

module.exports = dishRouter