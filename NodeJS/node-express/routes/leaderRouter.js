const express = require('express')

const leaderRouter = express.Router()

leaderRouter
  .route('/')
  .get((req, res, next) => {
    res.json({
      message: 'Will send details of all the leaders to you!',
    })
  })
  .post((req, res, next) => {
    res.status(201).json({
      message: `Will add the details of leader specified in request body`,
    })
  })

leaderRouter
  .route('/:leaderId')
  .get((req, res, next) => {
    res.status(200).json({
      leader: `Will send details of leader with id: ${req.params.leaderId}`,
    })
  })
  .put((req, res, next) => {
    res.status(200).json({
      message: `Updating leader with id: ${req.params.leaderId}`,
    })
  })
  .delete((req, res, next) => {
    res.status(200).json({
      message: `Deleting leader with id: ${req.params.leaderId}`,
    })
  })

module.exports = leaderRouter