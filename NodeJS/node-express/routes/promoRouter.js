const express = require('express')

const promoRouter = express.Router()

promoRouter
  .route('/')
  .get((req, res, next) => {
    res.json({
      message: 'Will send details of all the promotions to you!',
    })
  })
  .post((req, res, next) => {
    res.status(201).json({
      message: `Will add the promotion specified in the request body`,
    })
  })

promoRouter
  .route('/:promoId')
  .get((req, res, next) => {
    res.status(200).json({
      promotion: `Will send the details only for promotion with ID: ${req.params.promoId}`,
    })
  })
  .put((req, res, next) => {
    res.status(200).json({
      message: `Updating promotion with id: ${req.params.promoId}`,
    })
  })
  .delete((req, res, next) => {
    res.status(200).json({
      message: `Deleting promotion with id: ${req.params.promoId}`,
    })
  })

module.exports = promoRouter
