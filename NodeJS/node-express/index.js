const express = require('express')
const dishRouter = require('./routes/dishRouter')
const promoRouter = require('./routes/promoRouter')
const leaderRouter = require('./routes/leaderRouter')
const morgan = require('morgan')

const port = 3000

const app = express()

app.use(morgan('dev'))

app.use(express.static(__dirname + '/public'))


app.use(express.json())

app.use('/dishes', dishRouter)
app.use('/promotions', promoRouter)
app.use('/leaders', leaderRouter)

app.listen(port, () => {
  console.log(`Server running at localhost:3000/`)
})