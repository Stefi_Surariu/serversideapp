const mongoose = require('mongoose');
const timestamps = require('mongoose-timestamp');
const Schema = mongoose.Schema;

const favoriteSchema = new Schema(
  {
    dishes: [
      {
        type: Schema.Types.ObjectId,
        ref: 'Dish',
        unique: true,
      },
    ],
    postedBy: {
      type: Schema.Types.ObjectId,
      ref: 'User',
    },
  },
  {
    timestamps: true,
  }
);

var Favorites = mongoose.model('Favorites', favoriteSchema);

module.exports = Favorites;
